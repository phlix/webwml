#use wml::debian::template title="让网站翻译保持最新"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862"

<P>由于网页不是不变的，因此最好跟踪某个翻译所引用的原始版本，并使用此信息来检查\
自上次翻译以来哪些页面已被更改。此信息应以下方这种形式嵌入文档的顶部（虽然它位于
任何其他"use"头的下方）：

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>其中<var> git_commit_hash </var>是 git 提交的哈希值，它代表翻译文件所对应的\
原始（英语）文件的提交。您可以使用<code> git show </code>工具来获取特定提交的\
详细信息：<code>git show &lt;git_commit_hash&gt;</code>。如果您在 webwml 目录使用\
<kbd> copypage.pl </kbd>脚本，那么<code> translation-check </code>行会被自动\
添加到您翻译的页面的第一个版本中，并指向当时存在的原始文件的版本。</p>

<p>即使原始语言（英语）文件已被更改，某些翻译在相当长的时间内可能也不会得到更新。\
由于进行了内容协商，被翻译的页面的读者可能不知道这一点并因此错过在原文件的新版本\
中引入的重要信息。<code> translation-check </code>模板包含用于检查您的翻译是否\
过时的代码，并输出相应的适当消息警告用户。 </p>

<P>以下是您可以在<code> translation-check </code>行使用的更多参数：

<dl>
 <dt><code>original="<var>language</var>"</code>
 <dd><var> language </var>代表您翻译的原文件语言的名称，如果该语言不是英语。该\
 名称必须与 VCS 中的顶级子目录相对应，也需要与<code> languages.wml </code>模板中的\
 名称相符。

 <dt><code>mindelta="<var>number</var>"</code>
 <dd>这定义了该翻译文件被认为是<strong>老化</strong>所需要的最小 git 修订版本数。\
 默认值为<var>1</var>。对于不太重要的页面，请将其设置为<var>2</var>，这意味着在\
 将其翻译视为老化之前需要进行两次更改。

 <dt><code>maxdelta="<var>number</var>"</code>
 <dd>这定义了该翻译文件被认为是<strong>过时</strong>所需要的最小 git 修订版本数。\
 默认值为<var>5</var>。对于非常重要的页面，请将其设置为较小的数字。值为<var>1</var>\
 代表每次更改都会导致其翻译被标记为过时。
</dl>

<p>跟踪翻译文件的年龄还可以使我们获得<a href="stats/">翻译统计信息</a>，一份过时\
翻译的总体报告（以及指向文件之间差异对比的实用链接），还有根本没有翻译的页面的\
列表。目的是帮助翻译人员并吸引新人前来提供帮助。 </p>

<p>为避免向我们的用户提供严重过时的信息，自原始页面更改后六个月内未更新的翻译文件\
将被自动清除。请查看<a href="https://www.debian.org/devel/website/stats/">\
过时翻译清单</a>以查找可能被清除的页面。</p>

<P>另外，<kbd> check_trans.pl </kbd>脚本可在 webwml/目录中使用，用以向您显示一份\
需要更新的页面的报告：

<pre>
check_trans.pl <var>language</var>
</pre>

<P>其中<var> language </var>您的翻译的目录名称，例如，“swedish”。

<P>缺少翻译的页面将显示为“<code>Missing <var>filename</var></code>”，与原始文件相比\
不为最新的页面将显示为
“<code>NeedToUpdate <var>filename</var> to version <var>XXXXXXX</var></code>”。

<P>如果您想要查看确切的更改，可以通过在上面的指令中添加<kbd> -d </kbd>命令行参数\
来获得相关的差异对比。</p>

<P>如果您想要忽略有关缺少翻译的警告（例如，对于旧新闻），则可以在您想要隐藏警告的\
目录中创建一个名为<code> .transignore </code>的文件，在该文件中列出所有您不打算翻译\
的文件，一行一个。

<p>
您也可以使用一个与<kbd> check_trans.pl </kbd>相似的脚本来跟踪邮件列表描述的翻译。\
请阅读<code> check_desc_trans.pl </code>脚本中的注释以获取相关帮助。
</p>
