<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The patch applied for ntfs-3g to fix <a href="https://security-tracker.debian.org/tracker/CVE-2015-3202">CVE-2015-3202</a> in DLA 226-1 was
incomplete. This update corrects that problem. For reference the
original advisory text follows.</p>

<p>Tavis Ormandy discovered that NTFS-3G, a read-write NTFS driver for
FUSE, does not scrub the environment before executing mount or umount
with elevated privileges. A local user can take advantage of this flaw
to overwrite arbitrary files and gain elevated privileges by accessing
debugging features via the environment that would not normally be safe
for unprivileged users.</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in ntfs-3g version 1:2010.3.6-1+deb6u2</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2015/dla-226-2.data"
# $Id: $
