<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in the Xen hypervisor. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9932">CVE-2016-9932</a> (XSA-200)

    <p>CMPXCHG8B emulation allows local HVM guest OS users to obtain sensitive
    information from host stack memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7995">CVE-2017-7995</a>

    <p>Description
    Xen checks access permissions to MMIO ranges only after accessing them,
    allowing host PCI device space memory reads.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8903">CVE-2017-8903</a> (XSA-213)

    <p>Xen mishandles page tables after an IRET hypercall which can lead to
    arbitrary code execution on the host OS. The vulnerability is only exposed
    to 64-bit PV guests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8904">CVE-2017-8904</a> (XSA-214)

    <p>Xen mishandles the <q>contains segment descriptors</q> property during
    GNTTABOP_transfer. This might allow PV guest OS users to execute arbitrary
    code on the host OS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8905">CVE-2017-8905</a> (XSA-215)

    <p>Xen mishandles a failsafe callback which might allow PV guest OS users to
    execute arbitrary code on the host OS.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-8.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-964.data"
# $Id: $
