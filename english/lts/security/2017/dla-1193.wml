<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A file disclosure vulnerability was discovered in roundcube, a skinnable
AJAX based webmail solution for IMAP servers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16651">CVE-2017-16651</a>

     <p>An authenticated attacker can take advantage of this flaw to read
     roundcube's configuration files by uploading a specially crafted
     file attachment.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.7.2-9+deb7u9.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1193.data"
# $Id: $
