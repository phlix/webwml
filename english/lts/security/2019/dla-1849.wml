<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Fang-Pen Lin discovered a stack-based buffer-overflow flaw in ZeroMQ, a
lightweight messaging kernel library. A remote, unauthenticated client
connecting to an application using the libzmq library, running with a
socket listening with CURVE encryption/authentication enabled, can take
advantage of this flaw to cause a denial of service or the execution of
arbitrary code.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.0.5+dfsg-2+deb8u2.</p>

<p>We recommend that you upgrade your zeromq3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1849.data"
# $Id: $
