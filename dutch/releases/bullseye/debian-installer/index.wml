#use wml::debian::template title="Debian &ldquo;bullseye&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="0123585433dd4a52777010dc97d877ac4e2cb8ee"

<h1>Debian installeren <current_release_bullseye></h1>

<if-stable-release release="bookworm">
<p><strong>Debian 11 is vervangen door
<a href="../../bookworm/">Debian 12 (<q>bookworm</q>)</a>. Sommige van de
installatie-images hieronder zijn mogelijk niet langer beschikbaar of werken
niet meer. Het wordt aanbevolen om in de plaats daarvan bookworm te installeren.
</strong></p>
</if-stable-release>

<if-stable-release release="buster">
<p>
Raadpleeg voor installatie-images en documentatie over hoe u <q>bullseye</q>
(dat momenteel de Testing-distributie is) kunt installeren,
<a href="$(HOME)/devel/debian-installer/">de pagina van Debian-Installer</a>.
</if-stable-release>

<if-stable-release release="bullseye">
<p>
<strong>Voor het installeren van Debian</strong> <current_release_bullseye>
(<em>bullseye</em>) kunt u een van de volgende images downloaden (alle i386 en amd64
cd/dvd-images zijn ook bruikbaar op USB-stick):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst cd-image (meestal 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige cd-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige dvd-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, flexibele usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Als op uw systeem bepaalde hardware <strong>vereist dat niet-vrije firmware
geladen wordt</strong>  samen met het stuurprogramma voor het apparaat, dan kunt u een van de
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">\
tar-archieven met gangbare firmware-pakketten</a> gebruiken of een
<strong>niet-officieel</strong> image downloaden dat deze
<strong>niet-vrije</strong> firmware bevat. Instructies over het gebruik van
tar-archieven en algemene informatie over het laden van firmware tijdens een
installatie kunt u vinden in de
<a href="../amd64/ch06s04">Installatiehandleiding</a>.
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (gewoonlijk 240-290 MB) <strong>niet-vrije</strong>
cd-images <strong>met firmware</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <li>
	Voor het downloaden van volledige cd- of dvd-images wordt het gebruik van
	BitTorrent of jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de cd- of dvd-set beschikbaar als ISO-bestand of via BitTorrent.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	De multi-arch <em>cd</em>-images zijn bedoeld voor i386/amd64; de
    installatie is vergelijkbaar met een installatie met een netinst-image
    voor één enkele architectuur.
    </li><li>
	Het multi-arch <em>dvd</em>-image is bedoeld voor i386/amd64; de
	installatie is vergelijkbaar met een installatie met een
	volledig cd-image voor één enkele architectuur. De dvd bevat ook
	al de broncode voor de opgenomen pakketten.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt>  en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze <a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:

<ul>
<li><a href="../installmanual">Bullseye Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met gekende problemen in het installatiesysteem van
Debian <current_release_bullseye>. Indien u bij het installeren van Debian op
een probleem gestoten bent en dit probleem hier niet vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u het probleem beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata voor release 11.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Voor de volgende release van Debian wordt gewerkt aan een verbeterde versie
van het installatiesysteem, die u ook kunt gebruiken om bullseye te installeren.
Raadpleeg voor de concrete informatie
<a href="$(HOME)/devel/debian-installer/">de pagina van het Debian-Installer project
Improved versions of the installation system are being developed
for the next Debian release, and can also be used to install bullseye.
For details, see
<a href="$(HOME)/devel/debian-installer/">the Debian-Installer project
page</a>.
</p>
</if-stable-release>
