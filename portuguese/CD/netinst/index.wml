#use wml::debian::cdimage title="Instalação via rede a partir de um CD mínimo"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<p>Um CD de <q>instalação via rede</q> ou <q>netinst</q> é um único CD que
permite-lhe instalar todo o sistema operacional. Este único CD
contém apenas a quantidade mínima de software para começar a
instalação e obter os outros pacotes através da Internet.</p>

<p><strong>O que é melhor para mim &mdash; o CD-ROM inicializável mínimo
ou os CDs completos?</strong> Depende, mas achamos que em muitos casos
a imagem do CD mínimo é melhor &mdash; antes de mais nada, você só vai baixar
os pacotes que selecionar para a instalação na sua máquina, o que
economiza tempo e banda. Por outro lado, os CDs completos são mais
úteis quando for instalar em mais de uma máquina, ou máquinas sem
acesso livre à Internet.</p>

<p><strong>Quais tipos de conexões de rede são suportadas
durante a instalação?</strong>
A instalação via rede assume que você tenha acesso à Internet. Várias formas
diferentes são suportadas para isso, como uma conexão discada analógica PPP,
ethernet, WLAN (com algumas restrições), mas ISDN não é suportada &mdash;
desculpe!</p>

<p>As seguintes imagens inicializáveis do CD mínimo estão disponíveis para
baixar:</p>

<ul>

  <li>Imagens <q>netinst</q> oficiais para a distribuição estável
  (<q>stable</q>) &mdash; <a href="#netinst-stable">veja abaixo</a></li>

  <li>Imagens para a distribuição teste (<q>testing</q>), tanto as
  construções diárias como os snapshots que estão funcionando, veja a
  <a href="$(DEVEL)/debian-installer/">página do
  Debian-Installer</a>.</li>

</ul>


<h2 id="netinst-stable">Imagens netinst oficiais para a distribuição
estável (<q>stable</q>)</h2>

<p>Com até 300&nbsp;MB de tamanho, essa imagem contém o instalador e
um conjunto reduzido de pacotes, o que possibilita a instalação de um sistema
(muito) básico.</p>

<div class="line">
<div class="item col50">
<p><strong>Imagem do CD netinst (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>Imagem do CD netinst (geralmente entre 150-300 MB, varia por arquitetura)</strong></p>
    <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Para informações sobre o que são estes arquivos e como utilizá-los, por favor
consulte o <a href="../faq/">FAQ</a>.</p>

<p>Uma vez que tenha baixado uma dessas imagens, não esqueça de dar uma olhada
nas <a href="$(HOME)/releases/stable/installmanual">informações detalhadas
sobre o processo de instalação</a>.</p>

# Tradutores: o seguinte parágrafo existe (nesta ou em uma forma semelhante) várias vezes nos webwml,
# portanto, tente manter as traduções consistentes. Veja:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Imagens não oficiais netinst com firmwares não livres incluídos</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Se algum hardware em seu sistema <strong>requer que firmware não livre seja
carregado</strong> com o controlador do dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
arquivos tarball de pacotes de firmware comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que inclui esses firmwares <strong>não livres</strong>.
Instruções de como usar os arquivos tarball e informações gerais sobre como
carregar um firmware durante uma instalação podem ser encontradas no
<a href="../releases/stable/amd64/ch06s04">guia de instalação</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imagens
de instalação não oficiais para <q>estável (stable)</q> com firmwares
incluídos</a>
</p>
</div>
