#use wml::debian::template title="Coordenação de times l10n"
#use wml::debian::translation-check translation="1469899244087c9d456baf8dc6d1e40a328c9b34" maintainer="Thiago Pezzo (Tico)"


<h1>Pseudo-URLs</h1>

<p>
O programa que acompanha as listas debian-l10n-* consegue entender as
pseudo-URLs colocadas no cabeçalho de assunto.
As pseudo-URLs precisam ter a seguinte forma.
</p>
<div class="center"><code>[&lt;estado&gt;]&nbsp;&lt;tipo&gt;://&lt;pacote&gt;/&lt;arquivo&gt;</code></div>

<p>
O <i>estado</i> deve ser um dos seguintes:
TAF, MAJ, ITT, RFR, LCFC, BTS#&lt;número do bug&gt;, DONE ou HOLD.
</p>

<dl>
  <dt>
    <tt>TAF</tt> (Travail À Faire) (Trabalho a ser feito)
  </dt>
  <dd>
    Enviado por um(a) <strong>coordenador(a)</strong> (não por um(a)
    participante qualquer da lista) para indicar um documento que precisa ser
    trabalhado.
  </dd>
  <dt>
    <tt>MAJ</tt> (Mise À Jour) (Atualizar)
  </dt>
  <dd>
    Enviado por um(a) <strong>coordenador(a)</strong> (não por um(a)
    participante qualquer da lista) para indicar que existe um documento que
    precisa ser atualizado e que o trabalho está reservado para um(a)
    tradutor(a) anterior.
  </dd>
  <dt>
    <tt>ITT</tt> (Intent To Translate) (Intenção de traduzir)
  </dt>
  <dd>
    Indica que você planeja trabalhar na tradução; usado para evitar
    retrabalho.<br />
    Se você enviar uma mensagem <tt>[ITT]</tt> e alguém também enviar outra
    mensagem <tt>[ITT]</tt> para o mesmo arquivo, por favor, envie uma nova
    mensagem imediatamente para a lista de discussão para informar a esta
    pessoa que você tem a prioridade. O objetivo é evitar trabalho inútil.
  </dd>
  <dt>
    <tt>RFR</tt> (Request For Review) (Requisição de revisão)
  </dt>
  <dd>
    Um esboço de tradução segue anexado. Outras pessoas da lista são solicitadas
    a verificá-lo em busca de erros e enviar uma resposta (possivelmente fora da
    lista se nenhuma falha for encontrada).<br />
    Pode-se enviar novos RFRs se mudanças substanciais forem realizadas.
  </dd>
  <dt>
    <tt>ITR</tt> (Intent To Review) (Intenção de revisar)
  </dt>
  <dd>
    Usado para evitar que LCFCs sejam enviados quando ainda existem revisões
    pendentes.<br />
    Utiliza-se principalmente quando você acredita que sua revisão só ficará
    pronta dentro de alguns dias (porque a tradução é grande, ou você não tem
    tempo até o fim de semana, etc.).<br />
    No corpo do e-mail deve-se indicar quando pode-se esperar a revisão.<br />
    Observe que a pseudo-URL ITR é ignorada pelo robô que acompanha a lista de
    discussão.<br />
  </dd>
  <dt>
    <tt>LCFC</tt> (Last Chance For Comment) (Última chance para comentários)
  </dt>
  <dd>
    Indica que a tradução foi terminada, com as alterações do processo de
    revisão incorporadas, e que a tradução será enviada para seu lugar
    apropriado.<br />
    Pode ser enviado quando não existem mais ITRs e se a discussão que seguiu-se
    ao último RFR já terminou faz alguns dias.<br />
    Não deve ser enviado antes que uma revisão seja feita, pelo menos.
  </dd>
  <dt>
    <tt>BTS#&lt;número do bug&gt;</tt> (Bug Tracking System) (Sistema de
        acompanhamento de bugs)
  </dt>
  <dd>
    Usado para registrar um número de bug uma vez que você tenha submetido a
    tradução para o BTS.<br />
    O robô que acompanha a lista de discussão verificará se um relatório de bug
    aberto foi corrigido ou fechado.
  </dd>
  <dt>
    <tt>DONE</tt>
  </dt>
  <dd>
    Usado para fechar uma discussão (thread) uma vez que a tradução tenha
    terminado; útil se a tradução não foi enviada para o BTS.
  </dd>
  <dt>
    <tt>HOLD</tt>
  </dt>
  <dd>
    Usado para colocar a tradução em suspensão (hold); por exemplo, quando
    outras alterações estão previstas (se existem erros sobre a tradução no
    pacote ou a tradução está disponível em algum outro lugar).<br />
    O objetivo é evitar trabalho inútil.
  </dd>
</dl>

<p>
<i>tipo</i> pode ser qualquer coisa que indique o tipo do documento, como
po-debconf, po, po4a ou wml.
</p>

<p>
<i>pacote</i> é o nome do pacote do qual o documento se origina.
Por favor, use <i>www.debian.org</i> ou não use nada para arquivos WML do site
web do Debian.
</p>

<p>
<i>arquivo</i> é o nome do arquivo do documento; pode conter informações
adicionais para identificação única do documento, como o caminho para o arquivo.
Normalmente é um nome como <i>lc</i>.po, onde <i>lc</i> é o código do idioma
(por exemplo, <i>de</i> para alemão ou <i>pt_BR</i> para português brasileiro).
</p>

<p>
A estrutura do <i>arquivo</i> depende do tipo escolhido e, claro, do idioma.
Em princípio, é somente um identificador, mas como é utilizado nas páginas web
para rastrear o estado das traduções, recomenda-se que siga o esquema abaixo.
</p>

<ul>
<li><code>po-debconf://nome-do-pacote/lc.po</code> (para interface de configuração do instalador)</li>
<li><code>po://nome-do-pacote/caminho-no-pacote-fonte/lc.po</code> (para arquivo po clássico)</li>
<li><code>po4a://nome-do-pacote/caminho-no-pacote-fonte/lc.po</code> (para documentação convertida para formato po)</li>
<li><code>wml://caminho_sob_idioma_no_CVS</code> (para páginas do site web)</li>
<li><code>ddp://documento/nome-do-arquivo.po</code> (para documentação Debian)</li>
<li><code>xml://guia-de-instalação/idioma/caminho-no-pacote-fonte/arquivo.xml</code> (para guia de instalação)</li>
</ul>

<p>
O estado do BTS tem algo de especial; ele registra o número do bug para que
o robô-l10n possa rastrear o estado da tradução uma vez que foi submetida para o
BTS; isso acontece ao verificar se algum dos relatórios de bugs abertos foram
fechados. Desse modo, um exemplo seria a lista debian-l10n-spanish usar:
</p>
<div class="center"><code>[BTS#123456] po-debconf://cups/es.po</code></div>

<p>
Se você tem a intenção de traduzir vários pacotes, você pode fazer um ITT de
todos os pacotes ao mesmo tempo. Um exemplo (para a lista debian-l10n-danish):
</p>
<div class="center"><code>[ITT] po-debconf://{cups,courier,apache2}/da.po</code></div>
<p>
Coloque todos os pacotes entre chaves e separe-os com vírgulas. Sem espaços!
</p>