#use wml::debian::template title="Etapa 1: candidatura" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="0f3581ea8ff22d12b1665c2fb885ea6ccfe11f1c"

<p>As informações nesta página, embora públicas, são de interesse
principalmente dos(as) futuros(as) desenvolvedores(as) Debian.</p>

<h2>Etapa 1: candidatura</h2>

<p>Antes de se candidatar, o(a) futuro(a) desenvolvedor(a) deve
verificar se está preparado(a) para todas as partes das verificações. Para
tornar isso o mais fácil possível, os pré-requisitos mais importantes estão
listados abaixo:</p>

<ul>
 <li><h4>O(A) candidato precisa concordar com a filosofia do Debian</h4>
  <p>É necessário que o(a) candidato(a) tenha lido o
   <a href="$(HOME)/social_contract">contrato social</a> e a
   <a href="$(HOME)/social_contract#guidelines">definição Debian de software
   livre</a> e que concorda em cumpri-los em seus trabalhos relacionados ao
   Debian.
  </p>
 </li>

 <li><h4>A identidade do(a) candidato(a) precisa ser verificada</h4>
  <p>Isso é feito com pelo menos duas assinaturas de desenvolvedores(as) Debian
   na chave OpenPGP do(a) candidato(a). Também é altamente recomendável ter
   mais assinaturas de pessoas que estão bem conectadas na rede de confiança.
  </p>

  <p>Se a localização do(a) candidato(a) torna <em>impossível</em> a obtenção
   de uma chave assinada por outro(a) desenvolvedor(a) Debian, uma foto
   digitalizada da sua carteira de motorista ou passaporte assinado com sua
   chave GPG pode ser aceita como um método alternativo de identificação.
  </p>
 </li>

 <li><h4>O(A) candidato(a) precisa ser capaz de desempenhar suas funções como
  desenvolvedor(a)</h4>
  <p>Isso significa que o(a) candidato(a) deve ter experiência em empacotamento
   e manutenção de pacotes ou experiência em documentar ou traduzir, dependendo
   da área em que deseja trabalhar.
  </p>

  <p>É recomendado que o(a) candidato(a) tenha um(a)
   <a href="newmaint#Sponsor">padrinho/madrinha</a> para ajudá-lo(a) a conseguir
   isso.</p>
 </li>

 <li><h4>O(A) candidato(a) precisa ter lido a documentação do(a) desenvolvedor(a)</h4>
  <p>Mais tarde no processo, o(a)
  <a href="newmaint#AppMan">gestor(a) de candidatura</a> testará o conhecimento
  do(a) candidato(a) sobre conceitos descritos na
   <a href="$(DOC)/debian-policy/">política Debian</a>, na
   <a href="$(DOC)/developers-reference/">referência do(a) desenvolvedor(a)</a>,
   e no
   <a href="$(DOC)/maint-guide/">guia de novos(as) mantenedores(as)</a>, etc.
  </p>
 </li>

 <li><h4>O(A) candidato(a) precisa de tempo livre suficiente</h4>
  <p>Terminar a verificação de novo(a) membros(a) e se tornar um(a)
   desenvolvedor(a) Debian requer investir tempo regularmente. Manter pacotes
   no repositório principal é um grande compromisso e pode demandar bastante
   tempo.</p>
 </li>

 <li><h4>O(A) candidato(a) precisa ter um(a) <a href="newmaint#Advocate">\
  advogado</a></h4>
  <p>A fim de convencer um(a) desenvolvedor(a) a ser seu(sua) advogado(a),
   um(a) candidato deve se envolver no desenvolvimento do Debian &ndash; ajude
   a corrigir bugs abertos de pacotes existentes, adote um pacote órfão,
   trabalhe no instalador, empacote software novo e útil, escreva ou atualize a
   documentação etc.</p>
   
  <p>Observação: "advogado(a)" no contexto do Debian não é o(a) profissional da
   advocacia, mas alguém que "advoga" a favor de um(a) candidato(a) de maneira
   não profissional.</p>
 </li>
</ul>

<p>Uma vez que o(a) candidato(a) atenda às normas acima, ele(a) poderá
enviar sua <a href="https://nm.debian.org/public/newnm">candidatura de novo(a)
membro(a)</a>.</p>

<p>Depois de receber a candidatura, a
<a href="./newmaint#FrontDesk">secretaria</a> gerencia a candidatura. Assim que
alguém estiver disponível (isso pode levar algumas semanas), ele(a) será
designado(a) como <a href="./newmaint#AppMan">gestor(a) de candidatura</a>
para o(a) <a href="./newmaint#Applicant">candidato(a)</a> (ou seja, você).</p>

<p>Toda comunicação posterior deve acontecer entre o(a) candidato(a) e o(a)
gestor(a) de candidatura, mas se surgirem problemas, a recepção é o seu
ponto de contato primário.</p>

<hr>
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
