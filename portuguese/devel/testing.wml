#use wml::debian::template title="A distribuição Debian teste (&ldquo;testing&rdquo;)" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8900568ad5473cc15ea75f71f489b4f2b7ae659e"

<p>Para informações básicas aos(às) usuários(as) sobre a distribuição teste
(<q>testing</q>), veja a <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">FAQ do
Debian</a>.</p>

<p>Uma coisa importante a ser notada, tanto para usuários(as) regulares quanto
para desenvolvedores(as), é que as atualizações de segurança da <q>testing</q>
<strong>não são gerenciadas pela equipe de segurança</strong>. Para mais
informações veja a
<a href="../security/faq#testing">FAQ da equipe de segurança</a>.</p>

<p>Esta página cobre, fundamentalmente, os aspectos da <q>testing</q> que são
importantes para desenvolvedores(as) Debian.</p>

<h2>Como a <q>testing</q> funciona</h2>

<p>A distribuição <q>testing</q> é uma distribuição gerada automaticamente. Ela
é gerada a partir da distribuição instável (<q>unstable</q>) por um conjunto de
scripts que buscam mover pacotes que provavelmente não possuem bugs críticos ao
lançamento (<q>release-critical</q>). Eles o fazem de modo a garantir que as
dependências dos outros pacotes na <q>testing</q> sempre possam ser
satisfeitas.</p>

<p>Um pacote, de uma determinada versão, se moverá para a <q>testing</q> quando
ele satisfizer todos os seguintes critérios:</p>

<ol>
  <li>Ele precisa estar na instável (<q>unstable</q>) por 10, 5 ou 2 dias,
  dependendo da urgência do <q>upload</q>;</li>

  <li>Ele precisa estar compilado e atualizado em todas as arquiteturas
  nas quais ele foi anteriormente compilado na instável (<q>unstable</q>);</li>

  <li>Não pode haver nenhum bug crítico (<q>release-critical</q>), o que também
  se aplica à versão atualmente na <q>testing</q> (veja abaixo para
  <a href="#faq">mais informações</a>);</li>

  <li>Todas as suas dependências precisam ser satisfeitas, <em>ou</em>
  pelos pacotes que já estão na <q>testing</q>, <em>ou</em> pelo grupo de
  pacotes que serão instalados ao mesmo tempo;</li>

  <li>A operação de instalação do pacote na <q>testing</q> não deve quebrar
  qualquer outro pacote na <q>testing</q> (Veja abaixo para
  <a href="#faq">mais informações</a>.)</li>
</ol>

<p>Um pacote que satisfaz as três primeiras condições acima é considerado um
<q>candidato válido</q> (<q>valid candidate</q>).</p>

<p>O script de atualização mostra quando cada pacote deve mover-se da instável
(<q>unstable</q>) para a <q>testing</q>. A saída é dividida em duas partes:</p>

<ul>
  <li>As <a href="https://release.debian.org/britney/update_excuses.html">\
      justificativas de atualização (update excuses)</a>
      [<a href="https://release.debian.org/britney/update_excuses.html.gz">\
      compactadas com gzip</a>]:
      listam todos as versões candidatas dos pacotes e o estado básico de sua
      propagação para a <q>testing</q>; é um pouco mais curto e mais
      agradável que
  </li>
  <li>A <a href="https://release.debian.org/britney/update_output.txt">\
      saída de atualização (update output) </a>
      [<a href="https://release.debian.org/britney/update_output.txt.gz">\
      compactada com gzip</a>]:
      a saída completa, pouco refinada, dos scripts da <q>testing</q>
      conforme eles passam pelos candidatos
  </li>
</ul>

<h2><a name="faq">Dúvidas frequentes</a></h2>

# Nota para tradutores: estes dois primeiros itens são quase idênticos ao
# https://www.debian.org/doc/manuals/developers-reference/pkgs.html#faq

<h3><q>O que são bugs críticos ao lançamento (release-critical), e como
eles são contados?</q></h3>

<p>Todos os bugs de severidades altas são considerados
<em><a href="https://bugs.debian.org/release-critical/">críticos ao
lançamento</a></em> por padrão; atualmente, esses bugs são denominados de
<strong>críticos (critical)</strong>, <strong>graves (grave)</strong> e
<strong>sérios (serious)</strong>.</p>

<p>Presume-se que tais bugs tenham um impacto nas probabilidades do pacote
ser lançado com a versão estável (<q>stable</q>) do Debian: em geral, se um pacote tem bugs
críticos ao lançamento, ele não irá para a <q>testing</q>, e consequentemente
não será lançado na estável (<q>stable</q>).</p>

<p>
A contagem de bugs na <q>testing</q> é constituída de todos os bugs críticos ao
lançamento (<q>release-critical</q>) que, marcados para serem aplicados como
combinações <tt>pacote/versão</tt>, ficam disponíveis na <q>testing</q> para
lançamento em uma determinada arquitetura.</p>

<h3><q>Como a instalação de um pacote na testing poderia quebrar os
outros pacotes?</q></h3>

<p>A estrutura dos repositórios da distribuição é tal que ela pode conter
somente uma versão de um pacote; um pacote é definido por seu nome. Assim,
quando o pacote-fonte <tt>acmefoo</tt> é instalado na <q>testing</q>, junto com
seus pacotes binários <tt>acme-foo-bin</tt>, <tt>acme-bar-bin</tt>,
<tt>libacme-foo1</tt> e <tt>libacme-foo-dev</tt>, a versão antiga é
removida.</p>

<p>No entanto, a versão mais antiga pode também ter provido um pacote
binário com um <q>soname</q> antigo de uma biblioteca, como
<tt>libacme-foo0</tt>. Remover o <tt>acmefoo</tt> antigo removerá o
<tt>libacme-foo0</tt>, quebrando qualquer pacote que dependa dele.</p>

<p>Evidentemente, isto afeta principalmente pacotes que têm alterações
de pacotes binários em versões diferentes (assim sendo, principalmente
bibliotecas). No entanto, pacotes nos quais há dependências de versões
declaradas com as comparações ==, &lt;= ou &lt;&lt; também serão afetados.</p>

<p>Quando os pacotes binários vindos de um pacote-fonte se alteram deste modo,
todos os pacotes que dependam das bibliotecas antigas terão que ser
atualizados para depender dos binários novos. Como a instalação de tais
pacotes na <q>testing</q> quebram todos os pacotes que dependem deles na
<q>testing</q>, algum cuidado tem que ser tomado: todos os pacotes dependentes
precisam estar atualizados e prontos para serem instalados de modo que eles não
se quebrarão e, assim que tudo esteja pronto, geralmente é necessária a
intervenção manual do gerenciador de lançamento ou um assistente.</p>

<p>Se você está tendo problemas com grupos complicados de pacotes como estes,
contate as listas de discussão debian-devel ou a debian-release para receber ajuda.</p>

<h3><q>Eu ainda não entendo! Os scripts da testing dizem
que este pacote é um candidato válido, mas ele ainda não foi para a
testing.</q></h3>

<p>Isto tende a acontecer quando, de algum modo direto ou indireto,
instalar o pacote vai quebrar algum outro pacote.</p>

<p>Lembre-se de considerar as dependências do seu pacote. Suponha que
o seu pacote depende da libtool, ou libltdl<var>X</var>. Seu pacote não
irá para a <q>testing</q> até que a versão correta da libtool esteja pronta
para ir com ele.</p>

<p>Do mesmo modo, isso não irá ocorrer até que a instalação da libtool não
quebre pacotes que já estão na <q>testing</q>. Em outras palavras, até que todos os
outros pacotes que dependem da libltdl<var>Y</var> (onde <var>Y</var> é a
versão anterior) tenham sido recompilados, e todos os seus bugs críticos ao
lançamento estiverem corrigidos, etc, nenhum destes pacotes entrará na
<q>testing</q>.</p>

<p>É aqui que a
<a href="https://release.debian.org/britney/update_output.txt">saída de
texto</a>
[<a href="https://release.debian.org/britney/update_output.txt.gz">\
compactada com gzip</a>]
é útil: ela dá dicas (embora bastante resumidas) de quais pacotes quebram
quando um candidato válido é adicionado à <q>testing</q> (veja a
<a href="$(DOC)/manuals/developers-reference/pkgs.html#details">\
referência para desenvolvedores(as) para mais detalhes</a>).</p>

<h3><q>Por que algumas vezes é difícil ter pacotes <kbd>Architecture: all</kbd>
na testing?</q></h3>

<p>Se o pacote <kbd>Architecture: all</kbd> deve ser instalado, ele precisa
satisfazer suas dependências em <strong>todas</strong> as arquiteturas.
Se ele depende de determinados pacotes que compilam somente em um conjunto
limitado das arquiteturas do Debian, isto não será possível.</p>

<p>No entanto, e por enquanto, a <q>testing</q> ignorará a capacidade de
instalação dos pacotes <kbd>Architecture: all</kbd> em arquiteturas não-i386.
(<q>Isto é um hack grosseiro e eu não estou realmente feliz em ter feito isto,
mas que seja.</q> &mdash;aj)</p>

<h3><q>Meu pacote está parado porque ele está desatualizado em alguma
arquitetura. O que eu devo fazer?</q></h3>

<p>Verifique o estado de seu pacote no
<a href="https://buildd.debian.org/build.php">banco de dados de logs de
construção</a>. Se o pacote não pode ser construído, ele estará marcado como
<em>failed</em> (falha); investigue os logs e corrija todos os problemas
que foram causados pelas fontes do seu pacote.</p>

<p>Se você notar que alguma arquitetura construiu a versão nova do seu
pacote, mas ele não está aparecendo na saída dos scripts da <q>testing</q>,
você só precisa ser um pouco mais paciente até que o(a) mantenedor(a) do
respectivo buildd faça o upload dos arquivos para o repositório Debian.</p>

<p>Se você notar que algumas arquiteturas não construíram a nova versão de
seu pacote, apesar de você ter feito o upload de uma correção para uma
falha anterior, o motivo provavelmente é que ele está marcado como
esperando por dependências (Dep-Wait). Você também pode ver a lista dos
chamados <a href="https://buildd.debian.org/stats/">wanna-build states</a>
(estados quer construir) para se certificar.</p>

<p>Estes problemas acabam sendo corrigidos eventualmente, mas se você
está esperando por um período longo de tempo (digamos, duas semanas ou mais),
notifique o(a) mantenedor(a) do buildd do respectivo porte se tal contato
estiver documentado nas <a href="$(HOME)/ports/">páginas dos portes</a>, ou
notifique a lista de discussão do porte.</p>

<p>Se você explicitamente removeu uma arquitetura da lista <kbd>Architecture</kbd> no
arquivo de controle (<q>control</q>), e o pacote foi construído para aquela
arquitetura anteriormente, você precisa requisitar a remoção, no repositório,
do antigo pacote binário para esta arquitetura antes que seu pacote possa fazer
a transição para a <q>testing</q>. Você precisa reportar
um bug contra <q>ftp.debian.org</q> requisitando remoção de todos os pacotes
das arquiteturas removidas do repositório instável (<q>unstable</q>).
Geralmente, a lista do <q>porte</q> em questão deve ser informada como forma
de cortesia.</p>

<h3><q>Existem algumas exceções? Eu tenho certeza que o <tt>acmefoo</tt>
entrou na testing apesar de não satisfazer todos os requerimentos.</q></h3>

<p>Os(As) gerentes de lançamento podem sobrepujar as regras de dois modos:</p>

<ul>
  <li>Eles(as) podem decidir que a quebra causada pela instalação de uma nova
      biblioteca irá tornar as coisas melhores em vez de piores, e deixá-la
      entrar na <q>testing</q> junto com suas dependências.</li>
  <li>Eles(as) também podem remover manualmente pacotes da <q>testing</q> que
      ficariam quebrados, para que o novo material possa ser instalado.</li>
</ul>

<h3><q>Você pode dar um exemplo real e não trivial?</q></h3>

<p>Aqui está um: quando o pacote-fonte <tt>apache</tt> é instalado na
<q>testing</q>, junto com seus pacotes binários <tt>apache</tt>,
<tt>apache-common</tt>, <tt>apache-dev</tt> e <tt>apache-doc</tt>, a
versão antiga é removida.</p>

<p>No entanto, todos os pacotes de módulos do Apache dependem de
<code>apache-common (&gt;=<var>alguma-coisa</var>), apache-common
(&lt;&lt; <var>alguma-coisa</var>)</code>, assim esta alteração quebra
todas essas dependências. Consequentemente, todos os módulos Apache
precisam ser recompilados contra a nova versão do Apache para a
<q>testing</q> ser atualizada.</p>

<p>Vamos elaborar mais um pouco: depois que todos os módulos foram
atualizados na <q>instável</q> para trabalhar com o novo Apache, os scripts
da <q>testing</q> tentam <tt>apache-common</tt> e descobrem que ele quebra
todos os módulos Apache porque eles tem <code>Depends: apache-common
(&lt;&lt; <var>a versão atual</var>)</code>; e então tentam
<tt>libapache-<var>foo</var></tt> para descobrir que ele não instala
porque ele tem <code>Depends: apache-common (&gt;= <var>a versão
nova</var>)</code>.</p>

<p>No entanto, posteriormente eles aplicarão uma lógica diferente
(algumas vezes isso é requisitado por uma intervenção manual): eles ignorarão o
fato de que o <tt>apache-common</tt> quebra coisas, e continuarão com o que
funciona; se isto ainda não funcionar depois que nós fizermos
tudo que podíamos, tudo bem, mas talvez isto <strong>irá</strong>
funcionar. Posteriormente eles tentarão todos os pacotes
<tt>libapache-<var>foo</var></tt> e verificarão se eles realmente funcionam.</p>

<p>Depois que tudo tiver sido tentado, eles verificam quantos pacotes
foram quebrados, analisam se isto é melhor ou pior do que havia
originalmente, e aceitam tudo ou esquecem tudo. Você verá isto no
<tt>update_output.txt</tt> nas linhas <q><code>recur:</code></q>.</p>

<p>Por exemplo:</p>

<pre>
         recur: [<var>foo</var> <var>bar</var>] <var>baz</var>
</pre>

<p>basicamente diz que, <q>já tendo descoberto que <var>foo</var> e
<var>bar</var> tornam as coisas melhores, eu estou agora tentando
<var>baz</var> para ver o que acontece, mesmo sabendo que vai quebrar algo</q>.
As linhas do <tt>update_output.txt</tt> que começam com
<q><code>accepted</code></q> (aceito) indicam algo que parece tornar as
coisas melhores, e linhas <q><code>skipped</code></q> (ignorado) indicam algo
que deixam as coisas piores.</p>

<h3><q>O arquivo <tt>update_output.txt</tt> é completamente ilegível!</q></h3>

<p>Isto não é uma questão. ;-)</p>

<p>Vamos pegar um exemplo:</p>

<pre>
 skipped: cln (0) (150+4)
     got: 167+0: a-40:a-33:h-49:i-45
     * i386: ginac-cint, libginac-dev
</pre>

<p>Isto significa que se o <tt>cln</tt> entrar na <q>testing</q>,
<tt>ginac-cint</tt> e <tt>libginac-dev</tt> tornam-se não instaláveis
na <q>testing</q> no i386. Note que as arquiteturas são verificadas em ordem
alfabética e que somente os problemas na primeira arquitetura problemática
são mostrados &mdash; é por isso que a arquitetura alpha é mostrada tão
frequentemente.</p>

<p>A linha <q>got</q> inclui o número de problemas na <q>testing</q> nas
diferentes arquiteturas (até a primeira arquitetura onde um problema é
encontrado &mdash; veja acima). <q>i-45</q> significa que se o <tt>cln</tt>
fosse para a <q>testing</q>, haveria 45 pacotes não-instaláveis na i386.
Algumas das entradas acima e abaixo do <tt>cln</tt> mostram que havia 43
pacotes não-instaláveis na <q>testing</q>, na arquitetura i386 e naquele
momento.</p>

<p>A linha <q>skipped: cln (0) (150+4)</q> significa que ainda há 150 pacotes
para checar, após o pacote em questão, até que a verificação de todos os pacotes
seja completada; e que 4 pacotes já estão planejados para não serem atualizados,
pois quebrariam dependências. O <q>(0)</q> é irrelevante, você
pode ignorá-lo sem receios.</p>

<p>Observe que há várias verificações de todos os pacotes em uma rodada dos
scripts da <q>testing</q>.</p>

<p><em>Jules Bean organizou inicialmente as questões e respostas frequentemente
feitas.</em></p>
# Created: Sat Dec  8 12:44:29 GMT 2001

<h2>Informações Adicionais</h2>

<p>As páginas a seguir fornecem informações adicionais sobre o estado atual da
<q>testing</q> e a migração de pacotes da instável para a <q>testing</q>:</p>

<ul>
<li>Estatísticas sobre pacotes binários que estão desatualizados para
<a href="https://release.debian.org/britney/testing_outdate.txt"><q>testing</q></a>,
<a href="https://release.debian.org/britney/stable_outdate.txt">estável (<q>stable</q>)</a></li>
<li>Problemas de dependência em
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=testing"><q>testing</q></a>,
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=stable">estável (<q>stable</q>)</a></li>
</ul>

<p>Você pode estar interessado em ler um antigo
<a href="https://lists.debian.org/debian-devel-0008/msg00906.html">e-mail de
explicação</a>. Sua única grande falha é que ele não leva em conta o pool dos
pacotes, porque isto foi implementado por James Troup depois que o e-mail foi
escrito.</p>

<p>O código da <q>testing</q> está disponível em
<a href="https://release.debian.org/britney/update_out_code/">ftp-master</a>.</p>

<p><em>Anthony Towns leva os créditos pela implementação da <q>testing</q>.</em></p>
