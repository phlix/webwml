# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Templates webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-12-13 13:38+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Fecha"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Línea temporal"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Resumen"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominaciones"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Retiradas"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debate"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plataformas"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Proponente"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Proponente propuesta A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Proponente propuesta B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Proponente propuesta C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Proponente propuesta D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Proponente propuesta E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Proponente propuesta F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Proponente propuesta G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Proponente propuesta H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Apoyos"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Apoyos a la propuesta A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Apoyos a la propuesta B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Apoyos a la propuesta C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Apoyos a la propuesta D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Apoyos a la propuesta E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Apoyos a la propuesta F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Apoyos a la propuesta G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Apoyos a la propuesta H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Oposiciones"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Texto"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Propuesta A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Propuesta B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Propuesta C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Propuesta D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Propuesta E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Propuesta F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Propuesta G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Propuesta H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Opciones"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Proponente de la enmienda"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Apoyos a la enmienda"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Texto de la enmienda"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Proponente de la enmienda A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Apoyos a la enmienda A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Texto de la enmienda A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Proponente de la enmienda B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Apoyos a la enmienda B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Texto de la enmienda B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Proponente de la enmienda C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Apoyos a la enmienda C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Texto de la enmienda C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Enmiendas"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Procedimientos"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Mayoría necesaria"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Datos y estadísticas"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quórum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Mínima discusión"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Voto"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Foro"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultado"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Esperando&nbsp;Patrocinadores"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "En&nbsp;Discusión"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Votación&nbsp;en&nbsp;Curso"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decididas"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Retiradas"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Otras"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Página&nbsp;Principal&nbsp;de&nbsp;Votaciones"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Cómo"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Enviar&nbsp;Propuesta"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Enmendar&nbsp;una&nbsp;Propuesta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Apoyar&nbsp;una&nbsp;Propuesta"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Leer&nbsp;un&nbsp;Resultado"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Votar"
