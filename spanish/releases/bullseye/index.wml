#use wml::debian::template title="Información sobre Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86"

<if-stable-release release="bullseye">

<p>Debian <current_release_bullseye> se
publicó el <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 se publicó inicialmente el <:=spokendate('XXXXXXXX'):>."
/>
Esta versión incluye muchos cambios
importantes, que se describen en
nuestra <a href="$(HOME)/News/XXXX/XXXXXXXX">nota de prensa</a> y
en las <a href="releasenotes">notas de publicación</a>.</p>

#<p><strong>Debian 11 ha sido reemplazada por
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Las actualizaciones de seguridad han dejado de proporcionarse el <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Sin embargo, bullseye se beneficia del soporte a largo plazo (LTS, por sus siglas en inglés) hasta
#finales de xxxxx de 20xx. El LTS está limitado a i386, amd64, armel, armhf y arm64.
#El resto de arquitecturas ya no están soportadas en bullseye.
#Para más información, consulte la <a
#href="https://wiki.debian.org/LTS">sección LTS de la wiki de Debian</a>.
#</strong></p>

<p>Para obtener e instalar Debian, consulte
la página con <a href="debian-installer/">información para la instalación</a> y la
<a href="installmanual">guía de instalación</a>. Para actualizar desde una versión
anterior de Debian, consulte las instrucciones incluidas en las
<a href="releasenotes">notas de publicación</a>.</p>

### Activate the following when LTS period starts.
#<p>Arquitecturas soportadas durante el periodo de soporte a largo plazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Arquitecturas soportadas cuando se publicó inicialmente bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>En contra de nuestros deseos, puede haber algunos problemas en esta
versión, a pesar de haber sido declarada <em>estable</em>. Hemos hecho
<a href="errata">una lista de los principales problemas conocidos</a>, y siempre puede
<a href="reportingbugs">informarnos de otros</a>.</p>

<p>Por último, pero no menos importante, tenemos una lista de las <a href="credits">personas que han
contribuido</a> a hacer posible esta publicación.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>El nombre en código de la versión de Debian posterior a <a
href="../buster/">buster</a> es <q>bullseye</q>.</p>

<p>Esta versión empezó como una copia de buster, y en la actualidad está en un estado
llamado <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">en pruebas</a></q> («testing»).
Esto significa que no debería presentar fallos tan frecuentemente como las distribuciones
«inestable» o «experimental», porque a los paquetes solo se les permite pasar a formar parte de
esta distribución transcurrido un cierto periodo de tiempo, y siempre que
no tengan registrado contra ellos ningún fallo crítico para la publicación («release-critical»).</p>

<p>Tenga en cuenta que el equipo de seguridad <strong>no</strong> se encarga de las
actualizaciones de seguridad en <q>testing</q>. Como consecuencia, la distribución «en
pruebas» <strong>no</strong> recibe actualizaciones de seguridad con rapidez.
# Para más información, consulte el
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">anuncio</a>
# del equipo de seguridad de testing.
Le recomendamos modificar temporalmente sus
entradas en sources.list de testing a buster si
necesita soporte de seguridad. También puede consultar
la entrada sobre la distribución <q>testing</q> en las
<a href="$(HOME)/security/faq#testing">preguntas frecuentes sobre la seguridad</a>.</p>

<p>Puede haber disponible un <a href="releasenotes">borrador de las notas de publicación</a>.
También puede revisar los
<a href="https://bugs.debian.org/release-notes">añadidos propuestos a las notas de publicación</a>.</p>

<p>Para las imágenes de instalación y documentación sobre cómo instalar la distribución <q>testing</q>,
consulte <a href="$(HOME)/devel/debian-installer/">la página del instalador de Debian</a>.</p>

<p>Para saber más sobre cómo funciona la distribución <q>en pruebas</q>, revise
<a href="$(HOME)/devel/testing">la información sobre ella para desarrolladores</a>.</p>

<p>La gente pregunta con frecuencia si hay un único <q>medidor de avance</q> de la versión.
Lamentablemente no lo hay, pero podemos indicarle varios lugares
que describen cosas que es necesario resolver para que tenga lugar la publicación:</p>

<ul>
  <li><a href="https://release.debian.org/">Página genérica sobre el estado de publicación</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Fallos críticos para la publicación</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Fallos del sistema base</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Fallos en paquetes estándar y de tarea</a></li>
</ul>

<p>Además, el responsable de publicación envía informes del estado general
a la <a href="https://lists.debian.org/debian-devel-announce/">\
lista de correo «debian-devel-announce»</a>.</p>

</if-stable-release>
