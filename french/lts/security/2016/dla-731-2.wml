#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La mise à jour pour imagemagick publiée comme DLA-731-1 provoquait des
régressions lors du décodage des propriétés de certaines images. Des
paquets sont maintenant disponibles pour traiter ce problème. Pour
référence, le texte de l'annonce d'origine suit.</p>

<p>Plusieurs problèmes ont été découverts dans ImageMagick, un ensemble
populaire de programmes et de bibliothèques de manipulation d'images. Ils
incluent plusieurs problèmes dans le traitement de la mémoire qui peuvent
avoir pour conséquence une attaque par déni de service ou l'exécution de
code arbitraire par un attaquant doté du contrôle sur l'image d'entrée.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 8:6.7.7.10-5+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-731-2.data"
# $Id: $
