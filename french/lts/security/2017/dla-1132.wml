#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’hyperviseur Xen :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10912">CVE-2017-10912</a>

<p>Jann Horn a découvert que le traitement incorrect de transfert de page
pouvait aboutir à une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10913">CVE-2017-10913</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-10914">CVE-2017-10914</a>

<p>Jann Horn a découvert que des situations de compétition dans le traitement de
grant pouvait aboutir à une fuite d'informations ou à une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10915">CVE-2017-10915</a>

<p>Andrew Cooper a découvert qu'un comptage de références incorrect avec
une pagination « shadow » pourrait avoir pour conséquence une élévation des
privilèges.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10918">CVE-2017-10918</a>

<p>Julien Grall a découvert qu'un traitement d'erreur incorrect dans les
mappages de mémoire physique du système client avec la mémoire physique de
la machine peut avoir pour conséquence une élévation des privilèges, un déni
de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10920">CVE-2017-10920</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-10921">CVE-2017-10921</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-10922">CVE-2017-10922</a>

<p>Jan Beulich a découvert plusieurs emplacements où le comptage de
références sur les opérations de « grant-table » était incorrect, avec pour
conséquence une potentielle une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12135">CVE-2017-12135</a>

<p>Jan Beulich a découvert plusieurs problèmes dans le traitement
d'attributions transitives qui pourrait avoir pour conséquence un déni de
service et éventuellement une élévation des privilèges.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12137">CVE-2017-12137</a>

<p>Andrew Cooper a découvert que la validation incorrecte d'attributions
peut avoir pour conséquence une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12855">CVE-2017-12855</a>

<p>Jan Beulich a découvert le traitement incorrect de l'état d'attribution,
et en conséquence une information incorrecte du client sur le fait que
l'attribution n'est plus utilisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14316">CVE-2017-14316</a>

<p>Matthew Daley a découvert que le paramètre de nœud NUMA n’était pas vérifié,
ce qui peut aboutir dans une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14317">CVE-2017-14317</a>

<p>Eric Chanudet a découvert qu’une situation de compétition dans cxenstored
peut aboutir à une fuite d'informations ou une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14318">CVE-2017-14318</a>

<p>Matthew Daley a découvert qu’une validation incorrecte de « grant » peut
aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14319">CVE-2017-14319</a>

<p>Andrew Cooper a découvert que des vérifications insuffisantes de
« démappage » de « grant » peut aboutir à un déni de service et une élévation
des privilèges.</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.1.6.lts1-9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1132.data"
# $Id: $
