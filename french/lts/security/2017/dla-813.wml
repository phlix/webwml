#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wordpress, un outil de blog
web.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5488">CVE-2017-5488</a>

<p>Plusieurs vulnérabilités de script intersite (XSS) dans
wp-admin/update-core.php dans WordPress avant 4.7.1 permettent à des attaquants
distants d’injecter un script web arbitraire ou du HTML à l’aide du nom ou de
l’en-tête de version d’un greffon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5489">CVE-2017-5489</a>

<p>Une vulnérabilité de contrefaçon de requête intersite (CSRF) dans WordPress
avant 4.7.1 permet à des attaquants distants de détourner l’authentification
de victimes non précisées à l’aide de vecteurs impliquant un téléversement de
fichier Flash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5490">CVE-2017-5490</a>

<p>Une vulnérabilité de script intersite (XSS) dans la fonctionnalité de repli
pour le nom de thème dans wp-includes/class-wp-theme.php dans WordPress
avant 4.7.1 permet à des attaquants distants d’injecter un script web arbitraire
ou du HTML à l'aide d'un nom contrefait de répertoire de thème, relatifs
à wp-admin/includes/class-theme-installer-skin.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5491">CVE-2017-5491</a>

<p>wp-mail.php dans WordPress avant 4.7.1 peut permettre à des attaquants
distants de contourner des restrictions voulues de publication à aide d'un
serveur de courriels usurpé avec le nom mail.example.com.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5492">CVE-2017-5492</a>

<p>Une vulnérabilité de contrefaçon de requête intersite (CSRF) dans la
fonctionnalité de mode accessibilité d’édition de composant graphique dans
WordPress avant 4.7.1 permet à des attaquants distants de détourner
l’authentification de victimes non précisées pour des requêtes réalisant une
action d’accès à un composant graphique, relatives
à wp-admin/includes/class-wp-screen.php et wp-admin/widgets.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5493">CVE-2017-5493</a>

<p>wp-includes/ms-functions.php dans l’API Multisite WordPress, dans WordPress
avant 4.7.1, ne choisissait pas correctement les nombres aléatoires pour les clés,
ce qui facilitait pour des attaquants distants le contournement des restrictions
d’accès prévues, à l'aide d'un signup de site contrefait ou d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5610">CVE-2017-5610</a>

<p>wp-admin/includes/class-wp-press-this.php dans Press This, dans WordPress
avant 4.7.2, ne restreignait pas correctement la visibilité d’une interface
d’utilisateur taxonomy-assignment. Cela permet à des attaquants distants de
contourner les restrictions d’accès prévues par les termes de lecture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5611">CVE-2017-5611</a>

<p>Une vulnérabilité d’injection SQL dans wp-includes/class-wp-query.php, dans
WP_Query dans WordPress avant 4.7.2, permet à des attaquants distants d’exécuter
des commandes SQL arbitraires en exploitant la présence d’un greffon affecté
ou d’un thème gérant incorrectement un nom de type de publication contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5612">CVE-2017-5612</a>

<p>Une vulnérabilité de script intersite (XSS) dans
wp-admin/includes/class-wp-posts-list-table.php dans la table de liste des
publications, dans WordPress avant 4.7.2, permet à des attaquants distants
d’injecter un script web arbitraire ou du HTML à l'aide d'un extrait contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-813.data"
# $Id: $
