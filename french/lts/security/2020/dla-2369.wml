#use wml::debian::translation-check translation="349e3dbc1cb7bddb78f5b2d33a4b70fb334c90e1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été corrigées dans libxml2, la
bibliothèque XML de GNOME.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8872">CVE-2017-8872</a>

<p>Dépassement général de tampon dans la fonction htmlParseTryOrFinish.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18258">CVE-2017-18258</a>

<p>La fonction xz_head dans libxml2 permet à des attaquants distants de
provoquer un déni de service (consommation de mémoire) à l'aide d'un fichier
LZMA contrefait, car la fonction décodeur ne restreint pas l’utilisation de la
mémoire à ce qui est nécessaire pour un fichier valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14404">CVE-2018-14404</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL existe dans la fonction
xpath.c:xmlXPathCompOpEval() de libxml2 lors de l’analyse d’une expression XPath
non valable dans les conditions XPATH_OP_AND ou XPATH_OP_OR. Les applications
traitant des entrées au format XSL non approuvées pourraient être vulnérables
à une attaque par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14567">CVE-2018-14567</a>

<p>Si l’option --with-lzma est utilisée, cela permet à des attaquants distants
de provoquer un déni de service (boucle infinie) à l'aide d'un fichier XML
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19956">CVE-2019-19956</a>

<p>La fonction xmlParseBalancedChunkMemoryRecover possède une fuite de mémoire
concernant newDoc-&gt;oldNs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20388">CVE-2019-20388</a>

<p>Une fuite de mémoire a été découverte dans la fonction
xmlSchemaValidateStream de libxml2. Les applications qui utilisent cette
bibliothèque pourraient être vulnérables à une non liberation de mémoire
conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7595">CVE-2020-7595</a>

<p>Une boucle infinie dans xmlStringLenDecodeEntities peut provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24977">CVE-2020-24977</a>

<p>Lecture hors limites ne concernant que xmllint --htmlout.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.9.4+dfsg1-2.2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2369.data"
# $Id: $
