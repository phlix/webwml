#use wml::debian::translation-check translation="1c2da6148007d7faf105599eb68aa951075c2aed" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2101">CVE-2019-2101</a>

<p>Andrey Konovalov a découvert que le pilote USB Video Class (uvcvideo) ne
gérait pas systématiquement un champ de type dans les descripteurs de
périphérique. Cela pourrait aboutir à un dépassement de tampon basé sur le tas.
Cela pourrait être utilisé pour un déni de service ou éventuellement pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

<p>Amit Klein et Benny Pinkas ont découvert que la génération d’ID de paquet IP
utilisait une fonction de hachage faible incorporant une adresse virtuelle du
noyau. Dans Linux 3.16, cette fonction de hachage n’est pas utilisée pour des
ID d’IP mais pour d’autres buts dans la pile de réseau. Dans des configurations
personnalisées du noyau activant kASLR, cela peut affaiblir kASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13272">CVE-2019-13272</a>

<p>Jann Horn a découvert que le sous-système ptrace dans le noyau Linux ne
gèrait pas correctement les identifiants d'un processus qui veut créer une
relation ptrace, permettant à un utilisateur local d'obtenir les droits du
superutilisateur dans certains scénarios.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.70-1. Cette mise à jour corrige aussi une régression introduite
par le correctif original pour <a
href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>
(<a href="https://bugs.debian.org/930904">n° 930904</a>) et inclut d’autres
correctifs des mises à jour de l’amont pour stable.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux et linux-latest.
Vous devrez utiliser <q>apt-get upgrade --with-new-pkgs</q> ou <q>apt upgrade</q>
car les noms des paquets binaires ont changé.</p>


<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1862.data"
# $Id: $
