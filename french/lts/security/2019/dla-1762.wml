#use wml::debian::translation-check translation="ba832d784329e38ab2d7f613d7ac532a36bd5ad8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été corrigées dans les composants de systemd,
systemd-tmpfiles et pam_systemd.so.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18078">CVE-2017-18078</a>

<p>systemd-tmpfiles dans systemd essaie de prendre en charge les modifications
de propriété/permission pour les fichiers liés en dur même si le sysctl
fs.protected_hardlinks est désactivé. Cela permet à des utilisateurs locaux de
contourner les restrictions d’accès désirées à l’aide de vecteurs impliquant un
lien dur pour un fichier sur lequel l’utilisateur n’a pas le droit d’écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3842">CVE-2019-3842</a>

<p>pam_systemd ne vérifie pas correctement l’environnement avant d’utiliser la
variable XDG_SEAT. Il était possible pour un attaquant, dans quelques
configurations particulières, de régler une variable d’environnement XDG_SEAT
qui permette aux commandes d’être vérifiées en fonction des politiques de polkit
en utilisant l’élément <q>allow_active</q> plutôt que <q>allow_any</q>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 215-17+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1762.data"
# $Id: $
