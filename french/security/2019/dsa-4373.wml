#use wml::debian::translation-check translation="08e97d6a66338b9fb8da51eb27b4c3dde971c164" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans coTURN, un serveur
TURN et STUN de voix sur IP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

<p>Une vulnérabilité d'injection SQL a été découverte dans le portail web
d'administration de coTURN. Comme l'interface web d'administration est
partagée avec la production, il n'est malheureusement pas possible de
filtrer facilement les accès extérieurs et cette mise à jour de sécurité
désactive complètement l'interface web. Les utilisateurs doivent utiliser
à la place l'interface locale en ligne de commande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

<p>La configuration par défaut permet d'activer une redirection de boucle
locale non sécurisée. Un attaquant distant doté d'un accès à l'interface
TURN peut utiliser cette vulnérabilité pour obtenir un accès à des services
qui devraient être uniquement locaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

<p>La configuration par défaut utilise un mot de passe vide pour
l'interface locale en ligne de commande. Un attaquant doté d'un accès à la
console locale (soit un attaquant local, soit un attaquant distant tirant
avantage de
<a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
pourrait augmenter ses droits à ceux de l'administrateur du serveur coTURN.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 4.5.0.5-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets coturn.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de coturn, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
# $Id: $
